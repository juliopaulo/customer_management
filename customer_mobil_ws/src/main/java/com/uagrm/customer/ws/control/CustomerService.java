package com.uagrm.customer.ws.control;

import com.uagrm.customer.ws.dto.CustomerDto;
import com.uagrm.customer.ws.dto.CustomerListDto;

/**
 * 
 * @author jpcol
 *
 */
public interface CustomerService {

	/**
	 * 
	 * @param categoryDto
	 */
	void addCustomer(CustomerDto categoryDto) ;
	
	
	/**
	 * 
	 * @param start
	 * @param size
	 * @return
	 */
	CustomerListDto getCustomerQuery(Integer start, Integer size);
}
