package com.uagrm.customer.ws.interfaces;

import javax.ws.rs.core.Response;

import com.uagrm.customer.ws.dto.CustomerDto;
/**
 * 
 * @author jpcol
 *
 */
public interface CustomerResource {

	/**
	 * 
	 * @param customerDto
	 * @return
	 */
	Response addCustomer(CustomerDto customerDto);
	
	/**
	 * 
	 * @param start
	 * @param size
	 * @return
	 */
	Response getCustomerQuery(Integer start, Integer size);
	
}
