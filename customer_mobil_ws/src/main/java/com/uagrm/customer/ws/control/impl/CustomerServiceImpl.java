package com.uagrm.customer.ws.control.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uagrm.customer.control.CustomerRepository;
import com.uagrm.customer.entity.Customer;
import com.uagrm.customer.ws.control.CustomerService;
import com.uagrm.customer.ws.dto.CustomerDto;
import com.uagrm.customer.ws.dto.CustomerListDto;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

	@Inject 
	private CustomerRepository customerRepository;
	
	@Override
	@Transactional
	public void addCustomer(CustomerDto customerDto) {
		
		if(customerDto.getId()>0)
		{
			Customer customer = customerRepository.find(Customer.class, customerDto.getId());
			customer.setEmail(customerDto.getEmail());
			customer.setFirstName(customerDto.getFirstName());
			customer.setGender(customerDto.getGender());
			customer.setLastName(customerDto.getLastName());
			customer.setPhone(customerDto.getPhone());
			
			customer.setUpdatedAt(new Timestamp(new Date().getTime()));
			
			customerRepository.update(customer);
		}
		else
		{
			Customer customer = new Customer();
			customer.setEmail(customerDto.getEmail());
			customer.setFirstName(customerDto.getFirstName());
			customer.setLastName(customerDto.getLastName());
			customer.setGender(customerDto.getGender());
			customer.setPhone(customerDto.getPhone());
			
			customer.setUpdatedAt(new Timestamp(new Date().getTime()));
			customer.setCreatedAt(new Timestamp(new Date().getTime()));
			
			customerRepository.create(customer);
		}

	}

	@Override
	public CustomerListDto getCustomerQuery(Integer start, Integer size) {
		
		List<CustomerDto> result= new ArrayList<CustomerDto>();
		 
		List<Customer>  customerList = customerRepository.findWithNamedQuery(Customer.FIND_ALL, start, size);
		if(customerList!=null && customerList.size()>0)
		{
			for (Customer customer : customerList) {
				CustomerDto customerDto =new CustomerDto();
				customerDto.setEmail(customer.getEmail());
				customerDto.setFirstName(customer.getFirstName());
				customerDto.setLastName(customer.getLastName());
				customerDto.setGender(customer.getGender());
				customerDto.setPhone(customer.getPhone());
				customerDto.setId(customer.getId());
				
				result.add(customerDto);
			}
		}
		
		
		CustomerListDto customerListDto = new CustomerListDto();
		customerListDto.setCustomerList(result);
		
		return customerListDto;
	}

}
