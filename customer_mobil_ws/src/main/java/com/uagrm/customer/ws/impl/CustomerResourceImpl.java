/**
 * 
 */
package com.uagrm.customer.ws.impl;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.uagrm.customer.ws.control.CustomerService;
import com.uagrm.customer.ws.dto.CustomerDto;
import com.uagrm.customer.ws.dto.CustomerListDto;
import com.uagrm.customer.ws.interfaces.CustomerResource;

/**
 * @author jpcol
 *
 */
@Component
@Path("customer")
public class CustomerResourceImpl implements CustomerResource {

	@Inject
	private CustomerService customerService;
	
	@Override	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addCustomer(CustomerDto customerDto) {
		customerService.addCustomer(customerDto);
		return Response.ok().build();
	}


	@Override
	@GET
	@Path("query")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })	
	public Response getCustomerQuery(
			@QueryParam("start") @DefaultValue("0")Integer start,
			@QueryParam("size") @DefaultValue("10") Integer size) {
		
		CustomerListDto result = customerService.getCustomerQuery(start, size);
		
		return Response.ok().entity(result).build();
	}

}
