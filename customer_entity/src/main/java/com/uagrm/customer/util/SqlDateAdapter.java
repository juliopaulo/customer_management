package com.uagrm.customer.util;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class SqlDateAdapter extends XmlAdapter<String, Date> {

  @Override
  public Date unmarshal(String v) throws Exception {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    java.sql.Date sqlDate = null;
    try {
      java.util.Date convertedDate = dateFormat.parse(v);
      sqlDate = new java.sql.Date(convertedDate.getTime());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return sqlDate;
  }

  @Override
  public String marshal(Date v) throws Exception {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return dateFormat.format(v);
  }

}
