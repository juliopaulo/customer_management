package com.uagrm.customer.util;

import java.sql.Time;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * 
 * @author JulioPaulo
 *
 */
public class TimeAdapter extends XmlAdapter<String, Time> {

	@Override
	public Time unmarshal(String sTime) throws Exception {
		
		if(sTime!=null ){
			return Time.valueOf(sTime);			
		}
		return null;
	}

	@Override
	public String marshal(Time time) throws Exception {
		if(time!=null){
			return time.toString();	
		}		
		return "";
	}
}
