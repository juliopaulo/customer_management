package com.uagrm.customer.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class TimestampAdapter extends XmlAdapter<String, Timestamp> {
  public String marshal(Timestamp v) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return dateFormat.format(v);
  }

  public Timestamp unmarshal(String v) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    java.util.Date convertedDate = null;
    try {
      convertedDate = dateFormat.parse(v);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return new Timestamp(convertedDate.getTime());
  }
}
