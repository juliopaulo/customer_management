@XmlJavaTypeAdapters({@XmlJavaTypeAdapter(value = TimestampAdapter.class, type = Timestamp.class),
@XmlJavaTypeAdapter(value =SqlDateAdapter.class, type = Date.class),
@XmlJavaTypeAdapter(value =TimeAdapter.class, type = Time.class)})
package com.uagrm.customer.entity;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import com.uagrm.customer.util.SqlDateAdapter;
import com.uagrm.customer.util.TimeAdapter;
import com.uagrm.customer.util.TimestampAdapter;

