package com.uagrm.customer.control.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.uagrm.customer.control.CustomerRepository;
import com.uagrm.customer.entity.Customer;

@Repository
@Transactional(readOnly = true)
public class CustomerRepositoryImpl implements CustomerRepository {
	
    @PersistenceContext
    private EntityManager em;
    
	@Override
	@Transactional
	public Customer create(Customer t) {
		this.em.persist(t);
        this.em.flush();
        this.em.refresh(t);
        return t;
	}

	@Override
	public Customer find(Class<Customer> type, Object id) {
		return this.em.find(type, id);
	}

	@Override
	@Transactional
	public void delete(Class<Customer> type, Object id) {
		 Object ref = this.em.getReference(type, id);
	     this.em.remove(ref);	

	}

	@Override
	@Transactional
	public Customer update(Customer t) {
		return this.em.merge(t);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> findWithNamedQuery(String queryName, int pageIndex,
			int resultLimit) {
		return this.em.createNamedQuery(queryName)
        		.setMaxResults(resultLimit)
        		.setFirstResult(pageIndex*resultLimit)
        		.getResultList();  	
	}

}
