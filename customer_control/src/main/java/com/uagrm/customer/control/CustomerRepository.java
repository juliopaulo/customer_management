package com.uagrm.customer.control;

import com.uagrm.customer.entity.Customer;

/**
 * 
 * @author jpcol
 *
 */
public interface CustomerRepository extends CrudServices<Customer> {

}