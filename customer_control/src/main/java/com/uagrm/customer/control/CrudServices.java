package com.uagrm.customer.control;

import java.util.List;

/**
 * 
 * @author julio
 *
 */
public interface CrudServices<T> {

	T create(T t);
	
	T find(Class<T> type,Object id);
	
	void delete(Class<T> type,Object id);
	
	T update(T t);
	
	List<T> findWithNamedQuery(String queryName, int pageIndex, int resultLimit) ;
	
	
}
