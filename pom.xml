<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.uagrm.customer</groupId>
  <artifactId>customer_management</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <packaging>pom</packaging>
  <name>Customer Management</name>
  <description>Project to manage customer</description>
  
  <properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<spring.version>3.2.1.RELEASE</spring.version>
		<slf4j.version>1.7.1</slf4j.version>
		<logback.version>1.0.6</logback.version>
		<querydsl.version>2.9.0</querydsl.version>
		<hamcrest.version>1.3</hamcrest.version>
		<spring.data.jpa.version>1.3.0.RELEASE</spring.data.jpa.version>
		<hibernate.jpa.version>1.0.1.Final</hibernate.jpa.version>
		<hibernate.entity.manager.version>4.1.9.Final</hibernate.entity.manager.version>
		<mysql.version>5.1.26</mysql.version>
		<jersey.spring>1.18</jersey.spring>
		<javax.inject.version>1</javax.inject.version>
		<solrj.version>4.5.0</solrj.version>
		<spring.data.solr>1.3.0.RELEASE</spring.data.solr>
		<google.drive.api.version>v2-rev149-1.19.0</google.drive.api.version>
		<log4j.version>1.2.14</log4j.version>
		<gcm.server.version>1.0.2</gcm.server.version>
	</properties>
  
  <dependencyManagement>
		<dependencies>

			<dependency>
				<groupId>com.google.android.gcm</groupId>
				<artifactId>gcm-server</artifactId>
				<version>${gcm.server.version}</version>
			</dependency>


			<dependency>
				<groupId>mysql</groupId>
				<artifactId>mysql-connector-java</artifactId>
				<version>${mysql.version}</version>
			</dependency>
			<dependency>
				<groupId>javax.inject</groupId>
				<artifactId>javax.inject</artifactId>
				<version>${javax.inject.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-web</artifactId>
				<version>${spring.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-core</artifactId>
				<version>${spring.version}</version>
				<exclusions>
					<exclusion>
						<groupId>commons-logging</groupId>
						<artifactId>commons-logging</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-beans</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-context</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-aop</artifactId>
				<version>${spring.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-tx</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<!-- spring data jpa -->
			<dependency>
				<groupId>org.springframework.data</groupId>
				<artifactId>spring-data-jpa</artifactId>
				<version>${spring.data.jpa.version}</version>
			</dependency>

			<!-- Hibernate -->
			<dependency>
				<groupId>org.hibernate.javax.persistence</groupId>
				<artifactId>hibernate-jpa-2.0-api</artifactId>
				<version>${hibernate.jpa.version}</version>
			</dependency>

			<dependency>
				<groupId>org.hibernate</groupId>
				<artifactId>hibernate-entitymanager</artifactId>
				<version>${hibernate.entity.manager.version}</version>
				<scope>compile</scope>
			</dependency>

			<!-- Logging -->
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>${slf4j.version}</version>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>jcl-over-slf4j</artifactId>
				<version>${slf4j.version}</version>
				<scope>runtime</scope>
			</dependency>
			<dependency>
				<groupId>ch.qos.logback</groupId>
				<artifactId>logback-classic</artifactId>
				<version>${logback.version}</version>
				<scope>runtime</scope>
			</dependency>

			<!-- Test -->
			<dependency>
				<groupId>org.hamcrest</groupId>
				<artifactId>hamcrest-library</artifactId>
				<version>${hamcrest.version}</version>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>org.hamcrest</groupId>
				<artifactId>hamcrest-core</artifactId>
				<version>${hamcrest.version}</version>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit-dep</artifactId>
				<version>4.10</version>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-test</artifactId>
				<version>${spring.version}</version>
				<scope>test</scope>
			</dependency>
			<!-- JERSEY -->
			<dependency>
				<groupId>com.sun.jersey.contribs</groupId>
				<artifactId>jersey-spring</artifactId>
				<version>${jersey.spring}</version>
			</dependency>

			<dependency>
				<groupId>com.sun.jersey</groupId>
				<artifactId>jersey-client</artifactId>
				<version>${jersey.spring}</version>
			</dependency>

			<dependency>
				<groupId>com.sun.jersey</groupId>
				<artifactId>jersey-json</artifactId>
				<version>${jersey.spring}</version>
			</dependency>

			<dependency>
				<groupId>com.sun.jersey.contribs</groupId>
				<artifactId>jersey-multipart</artifactId>
				<version>${jersey.spring}</version>
			</dependency>


			<!-- JERSEY for json results -->
			<dependency>
				<groupId>com.sun.jersey</groupId>
				<artifactId>jersey-bundle</artifactId>
				<version>${jersey.spring}</version>
			</dependency>

			<!-- Solr dependencies -->
			<dependency>
				<artifactId>solr-solrj</artifactId>
				<groupId>org.apache.solr</groupId>
				<version>${solrj.version}</version>
				<type>jar</type>
				<scope>compile</scope>
			</dependency>

			<dependency>
				<groupId>org.springframework.data</groupId>
				<artifactId>spring-data-solr</artifactId>
				<version>${spring.data.solr}</version>
			</dependency>
			<dependency>
				<groupId>com.google.apis</groupId>
				<artifactId>google-api-services-drive</artifactId>
				<version>${google.drive.api.version}</version>
			</dependency>

			<dependency>
				<groupId>log4j</groupId>
				<artifactId>log4j</artifactId>
				<version>${log4j.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
	<build>
		<plugins>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.0</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.13</version>
			</plugin>

		</plugins>
	</build>
  
	<modules>
		<module>customer_entity</module>
		<module>customer_control</module>
		<module>customer_mobil_ws</module>
	</modules>
</project>